<?php
define("MAX_SIZE", 6); //Max Mb
define("UPLOAD_FOLDER", "assets/uploads");

class Upload
{
    private $file = null;
    private $title = null;
    private $error = null;
    private $filePath = null;

    function __construct($file, $title)
    {
        $this->file = $file;
        $this->title = $title;
        $this->uploadFile();
    }
    /*
    * 
    *   uploadFile()
    *   PARAMS: none
    */
    function uploadFile()
    {
        try {
            //Check if the folder exist is a valid picture and title
            //$_SERVER['DOCUMENT_ROOT'] return the folder of the server where is our APP
            if (!is_dir($_SERVER['DOCUMENT_ROOT'] . '/' . UPLOAD_FOLDER)) {
                if (!mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . UPLOAD_FOLDER))
                    throw new UploadError("Error: No se ha podido crear el directorio");
            }
            if (!is_writable($_SERVER['DOCUMENT_ROOT'] . '/' . UPLOAD_FOLDER))
                throw new UploadError("Error: No puedes escribir en el directorio");

            // Check if file was uploaded without errors
            if ($this->file["error"] != 0)
                throw new UploadError("Error: " . $this->file["error"]);

            //Define all data of the FILE
            $filename = $this->file["name"];
            $filesize = $this->file["size"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $uploadPath = UPLOAD_FOLDER . $filename;

            // Verify file extension (PDF)
            /*
            *  BLOQUE A: VERIFICA QUE SE SUBE UN ARCHIVO CON EXTENSION PDF Y DE TIPO MIME PDF
            *
            */
            if (!$ext) {
                throw new UploadError("Error: Selecciona un fichero valido");
                
            }

            // Verify file size - 5MB maximum
            $maxsize = MAX_SIZE * 1024 * 1024;
            if ($filesize > $maxsize)
                throw new UploadError("Error: File size is larger than the allowed limit.");

            // Check whether file exists before uploading it
            /*
            BLOQUE B: SUBE EL ARCHIVO A LA RUTA ABSOLUTA DE DESTINO. Si se ha subido correctamente se define el filePath
            */
        } catch (UploadError $e) {
            $this->error = $e -> getMessage();
        } catch (Exception $e) {
            $this->error = $e -> getMessage();
        }
    }
    /*
    * Esta función añadirá al archivo de base de datos de la asignatura la información del archivo subido.  Si no existe
    * La base de datos se creará en assets/uploads
    * addUploadToFile
    * $subject: Asignatura (base de datos) donde se guardará la información del archivo
    */
    function addUploadToFile($subject)
    {
    }
    function getError()
    {
        return $this->error;
    }
    function getFile()
    {
        return $this->filePath;
    }
}
